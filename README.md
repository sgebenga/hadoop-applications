

Hadoop Applications by Khayeni Ndlovu
====

![Alt text](https://s3.eu-west-2.amazonaws.com/sgebenga-hadoop-applications-image/bigdata-hadoop.png)

Dependencies
====
Java 1.8 , Maven , Hadoop v2.8

List of applications
====
* [Big data utils](https://bitbucket.org/sgebenga/generator) Utility application to generate sample Big Data
* [Word count](https://bitbucket.org/sgebenga/wordcount) 
* [Partitioner](https://bitbucket.org/sgebenga/partitioner) 
* [Custom key class](https://bitbucket.org/sgebenga/customkey)
* [Custom value class](https://bitbucket.org/sgebenga/combinerkeyvalue)
* [Map-side join](https://bitbucket.org/sgebenga/combinesidejoin)
* [Reduce-side join](https://bitbucket.org/sgebenga/reducesidejoin)
* [Combiner](https://bitbucket.org/sgebenga/combinerkeyvalue)
* [Comparator](https://bitbucket.org/sgebenga/reducesidejoin)
* [Sorting](https://bitbucket.org/sgebenga/)
* [Secondary sorting](https://bitbucket.org/sgebenga/)
* [MR unit](https://bitbucket.org/sgebenga/mrunit)
* [MapReduce Map Side](https://bitbucket.org/sgebenga/flightsdelay)
* [Custom MapReduce WritableComparable](https://bitbucket.org/sgebenga/examcohorts)
* [Custom File Input Format](https://bitbucket.org/sgebenga/inputformat)
* [Hadoop File Input Formats](https://bitbucket.org/sgebenga/inputformats)
* [Replicated Join](https://bitbucket.org/sgebenga/replicatedjoin)

Who do I talk to? 
====
* Repo owner : [Khayeni Ndlovu](https://www.linkedin.com/in/khayeni/)
